package programing;

public class Pattern14 {
    public static void main(String[] args) {
        int line=5;
        int star=1;

        for(int a=0;a<line;a++)
        {
            for(int b=0;b<star;b++)
            {
                System.out.print("*");
            }
            System.out.println();
            star++;
        }
    }
}
