package Array;

public class ArrayDemo7 {
    static String []product={"tv","projector","mobile"};
    static double[]cost={15000,25000,30000};
    static int []stock={25,10,50};

    void calculateBill(int choice,int qty)
    {
        boolean found=false;

        for(int a=0;a<product.length;a++)
        {
            if(choice==a && qty<=stock[a])
            {
                double total=qty*cost[a];
                stock[a]-=qty;

                System.out.println("total bill amt"+total);
                found=true;
            }
        }
        if(! found)
        {
            System.out.println("product out of stock");
        }
    }
}
