package Forloop;

import java.util.Scanner;

public class Forloop1
{
    public static void main(String[] args)
    {
        Scanner sc1 = new Scanner(System.in);
        System.out.println("Enter start point");
        int start = sc1.nextInt();
        System.out.println("Enter end point");
        int end = sc1.nextInt();


        for (int b= start; b<= end; b++)
        {
            if (b%2!= 0)
            {
                System.out.println(b);
            }

        }
    }
}
