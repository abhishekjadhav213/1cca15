package Series;

public class CountZero {
    public static void main(String[] args) {
        long a=11100001025500020l;
        int count=0;
        while(a!=0)
        {
            long r=(a%10);
            //or use this
            //int r= (int) (a%10);
            if(r==0)
                count++;
            a=a/10;

        }
        System.out.println(count);

    }
}
