package Series;

public class Recursion {
    static int sum = 0;

    public static void main(String[] args) {

        add(10);
    }

    public static void add(int a) {
        if (a > 0) {
            System.out.println(sum);
            sum += 2;
            a--;
            add(a);
        }

    }
}
